import 'package:intl/intl.dart';

class DateUtil {
  static String formatDateTime(DateTime date) {
    return DateFormat.yMMMd().format(date);
  }
}
